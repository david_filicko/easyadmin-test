<?php

namespace App\Controller;

use App\Entity\Article;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractCrudController
{
//    #[Route('/article', name: 'article')]
//    public function index(): Response
//    {
//        return $this->render('article/index.html.twig', [
//            'controller_name' => 'ArticleController',
//        ]);
//    }



    public static function getEntityFqcn(): string
    {
        return Article::class;
    }
}
