<?php

namespace App\Controller;

use App\Entity\Tag;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TagController extends AbstractCrudController
{
//    #[Route('/tag', name: 'tag')]
//    public function index(): Response
//    {
//        return $this->render('tag/index.html.twig', [
//            'controller_name' => 'TagController',
//        ]);
//    }


    public static function getEntityFqcn(): string
    {
        return Tag::class;
    }
}
